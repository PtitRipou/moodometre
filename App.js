import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import QuestionnaireComponent from './components/QuestionnaireComponent.js';
import AccueilComponent from "./components/AccueilComponent";
import DrawerNavigatorComponent from './components/DrawerNavigatorComponent.js';

export default function App() {
  return (
      <DrawerNavigatorComponent/>
    );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00e6e6',
  },
});

