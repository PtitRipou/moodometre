import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class BoutonComponent extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            text: 'Moodomètre'
        }
    }

    render(){
        return (
            <View style={styles.button}>
               <Button title={this.state.text}></Button> 
            </View>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        marginHorizontal : "20%",
        marginTop: 50,
    },
});

export default BoutonComponent