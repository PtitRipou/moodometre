import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, ImageBackground } from 'react-native';

class ActivitesComponent extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
       
    }

    
    
    render(){
        return (
            <View style = {styles.test}> 
            <ImageBackground source={this.props.act.image} style={styles.image}>
            <TouchableOpacity onPress= {() => alert(`${this.props.act.description}\n\nPrix estimé: ${this.props.act.prix}\n\nTemps estimé: ${this.props.act.temps}`)} style = {styles.test2} >
            <Text style={styles.text}>{this.props.act.nom}</Text>
            </TouchableOpacity>
            </ImageBackground>
            </View>
        )
    }
}



const styles = StyleSheet.create({
    text: {
      backgroundColor: 'rgba(0.5,0.5,0.5,0.3)',
      color: "white",
      fontSize: 25,
      textAlign: 'center',
      fontWeight: 'bold', 
    },
    test2: {
        flex: 1,
        borderColor: 'white',
        borderWidth: 5,
        padding: 30,
        alignContent: 'center',
    },
    test: {
        flex: 1,
        padding: 30,
        margin: 20,
    },
    image: {
        flex: 1,
        resizeMode: 'center',
        justifyContent: "center",
        

      },
    
});

export default ActivitesComponent
