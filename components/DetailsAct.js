import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

class DetailsAct extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
        }
    }

    render(){
        return (
            <View>
                <Text style={styles.titre}>Informations:</Text>
                <Text style={styles.text}> DetailsAct component</Text>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
      backgroundColor: "blue",
      color: "white",
      fontSize: 20
    },
    titre: {

    }
});

export default DetailsAct
