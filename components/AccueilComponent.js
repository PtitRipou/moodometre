import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import CitationComponent from "./CitationComponent";
import CalendrierComponent from "./CalendrierComponent";
import BoutonComponent from "./BoutonComponent";

class AccueilComponent extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            text: "Bienvenue sur le Moodomètre",
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.citation}>
                    <CitationComponent ></CitationComponent>
                </View>
                <View style={styles.accueil}>
                    <Text style={styles.text}>{this.state.text}</Text>
                    <BoutonComponent></BoutonComponent>
                </View>
                <View style={styles.calendrier}>
                    <CalendrierComponent></CalendrierComponent>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#00e6e6",
        flex: 1,
    },

    citation: {
        flex: 1,
        justifyContent: 'center',
    },

    accueil: {
        flex: 1,
        justifyContent: 'center',
        borderTopWidth: 2,
        borderBottomWidth: 2,
        borderColor: "#6666ff",
    },

    calendrier: {
        flex: 2,
        justifyContent: 'center',
    },
    
    text: {
        textAlign: 'center',
        color: "white",
        fontSize: 25,
    },
});

export default AccueilComponent