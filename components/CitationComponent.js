import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

class CitationComponent extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            dataSource: {},
        }
    }

    componentDidMount() {
        return fetch('https://api.quotable.io/random')
        .then((response) => response.json())
        .then((responseJson) => {
            setTimeout(() => {
                this.setState({ dataSource: responseJson })
            }, 1500); 
        })
    }

    render(){
        return (
            <View>
                <Text style={styles.content}>" {(this.state.dataSource.content) ? this.state.dataSource.content : <Text>Citation en chargement...</Text>} "</Text>
                <Text style={styles.author}>{(this.state.dataSource.author) ? this.state.dataSource.author : <Text>Auteur en chargement...</Text>}</Text>                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        textAlign: 'center',
        fontSize: 20,
        color: "white",
    },

    author: {
        marginTop: 15,
        textAlign: 'center',
        fontSize: 15,
        color: "white",
    },
    
    text: {
      backgroundColor: "blue",
      color: "white",
      fontSize: 20
    },
});

export default CitationComponent