import React, {useState} from 'react';
import { StyleSheet, Text, View,SafeAreaView } from 'react-native';
import Slider from '@react-native-community/slider';

class QuestionComponent extends React.Component {
    constructor(props){
        super(props)
        this.onSlideChange = this.onSlideChange.bind(this);

        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            sliderValue: 5
        }
    }

    onSlideChange(newValue){
        this.setState({sliderValue:newValue});
        this.props.onSliderValueChange({valeur:newValue, id:this.props.id});
    }

    render(){
        return (
            <SafeAreaView StyleSheet="styles">
                <Text style={styles.text}>{this.props.enonce}</Text>
                <Slider 
                    style={styles.slider}
                    value={5}
                    step={1}
                    minimumValue={0}
                    maximumValue={10}
                    minimumTrackTintColor="#FF00FF"
                    maximumTrackTintColor="#000000"
                    onValueChange={this.onSlideChange}
                />
                <Text style={styles.compteur}>{this.state.sliderValue}</Text>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    text: {
      color: "grey",
      fontSize: 20,
      textAlign:'center'
    },
    slider: {
        width:"100%"
    },
    compteur:{
        fontSize:50,
        textAlign:'center'
    }
});

export default QuestionComponent
