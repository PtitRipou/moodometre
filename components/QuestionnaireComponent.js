import React from 'react';
import { StyleSheet, Text, View, Button,FlatList } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import QuestionComponent from "./QuestionComponent";
import {AsyncStorage} from 'react-native';

class QuestionnaireComponent extends React.Component {
    constructor(props){
        super(props)

        this.onSliderValueChange = this.onSliderValueChange.bind(this);
        this.validerQuestionnaire = this.validerQuestionnaire.bind(this);

        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            nbQuestions:10,
            questions:[{enonce:"Avez-vous bien dormi ?", coef:{bonheur:0.0, detente:0.0,forme:0.6,humeur:0.4}, valeur:5},
                    {enonce:"Avez-vous envi de faire du sport ?",coef:{bonheur:0.0, detente:0.0,forme:0.7,humeur:0.3}, valeur:5},
                    {enonce:"Avez-vous bien déjeuné ?",coef:{bonheur:0.0, detente:0.4,forme:0.6,humeur:0.0}, valeur:5},
                    {enonce:"Avez-vous apprécié la journée d'hier ?",coef:{bonheur:1.0, detente:0.0,forme:0.0,humeur:0.0}, valeur:5},
                    {enonce:"Etes-vous plein d'énergie ?",coef:{bonheur:0.0, detente:0.3,forme:0.7,humeur:0.0}, valeur:5},
                    {enonce:"Etes-vous calme par rapport à la situation actuelle ?",coef:{bonheur:0.0, detente:1.0,forme:0.0,humeur:0.0}, valeur:5},
                    {enonce:"Vous sentez vous en sécurité dehors ?",coef:{bonheur:0.0, detente:1.0,forme:0.0,humeur:0.0}, valeur:5},
                    {enonce:"Avez-vous peu de travail aujourd'hui ?",coef:{bonheur:0.0, detente:0.6,forme:0.7,humeur:0.4}, valeur:5},
                    {enonce:"Ressentez-vous le besoin de sortir ?",coef:{bonheur:0.3, detente:0.3,forme:0.7,humeur:0.2}, valeur:5},
                    {enonce:"Vous sentez-vous supporté par vos proches (amis, familles...) ?",coef:{bonheur:1.0, detente:0.3,forme:0.0,humeur:0.0}, valeur:5},
                    {enonce:"La météo vous inspire-t-elle ?",coef:{bonheur:0.7, detente:0.0,forme:0.4,humeur:0.0}, valeur:5},
                    {enonce:"Etes-vous en harmonie avec vous même ?",coef:{bonheur:1.0, detente:0.0,forme:0.0,humeur:0.0}, valeur:5}
                    ],
            listQuestions:[],
            id:0,
            mood:{
                bonheur:0,
                detente:0,
                forme:0,
                humeur:0
            },
            coefTotMood:{
                bonheur:0,
                detente:0,
                forme:0,
                humeur:0 
            },
            fini:false
        }

        this.genererQuestions();
    }

    genererQuestions(){
        let nb = 0;
        let pris = [];
        let idQuestions = [];
        let listQuestions = [];
        let nbQuestionsFinal = Math.min(this.state.nbQuestions, this.state.questions.length);
        let i = 0;
        while(nb < nbQuestionsFinal){
            i = Math.floor(Math.random() * this.state.questions.length);
            if(!idQuestions.includes(i)){
                idQuestions.push(i);
                this.state.listQuestions.push(this.state.questions[i]);
                this.state.coefTotMood.bonheur += this.state.questions[i].coef.bonheur;
                this.state.coefTotMood.detente += this.state.questions[i].coef.detente;
                this.state.coefTotMood.forme += this.state.questions[i].coef.forme;
                this.state.coefTotMood.humeur += this.state.questions[i].coef.humeur;
                nb++;
            }
        }
    }

    onSliderValueChange(value){
        this.state.listQuestions[value.id].valeur = value.valeur;
    }

    validerQuestionnaire(data){
        this.state.mood.bonheur = 0;
        this.state.mood.detente = 0;
        this.state.mood.forme = 0;
        this.state.mood.humeur = 0;
        for(let i = 0; i < this.state.listQuestions.length; i++){
            let ques = this.state.listQuestions[i];
            this.state.mood.bonheur += ques.valeur * ques.coef.bonheur;
            this.state.mood.detente += ques.valeur * ques.coef.detente;
            this.state.mood.forme += ques.valeur * ques.coef.forme;
            this.state.mood.humeur += ques.valeur * ques.coef.humeur;
        }
        this.state.mood.bonheur /= 10*this.state.coefTotMood.bonheur;
        this.state.mood.detente /= 10*this.state.coefTotMood.detente;
        this.state.mood.forme /= 10*this.state.coefTotMood.forme;
        this.state.mood.humeur /= 10*this.state.coefTotMood.humeur;

        let vals = [this.state.mood.bonheur,this.state.mood.forme,this.state.mood.detente,this.state.mood.humeur];
        vals.sort();
        let colors = "#000000";
        let annexe;
        let moodDominant;
        if(vals[0] == this.state.mood.bonheur){
            annexe = this.state.mood.bonheur.toString(16);
            colors = "#00" + annexe + "00";
            moodDominant = "Bonheur";
        } else if (vals[0] == this.state.mood.forme){
            annexe = this.state.mood.forme.toString(16);
            colors = "#" + annexe + "0000";
            moodDominant = "Forme";
        } else if (vals[0] == this.state.mood.detente){
            annexe = this.state.mood.forme.toString(16);
            colors = "#0000" + annexe;
            moodDominant = "Détente";
        } else if (vals[0] == this.state.mood.humeur){
            annexe = this.state.mood.forme.toString(16);
            colors = "#8080" + annexe;
            moodDominant = "Humeur";
        }

        /*let storeData = async() => {
            try {
                //let date = new date();
                //let cle = date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
                //const jsonValue = JSON.stringify([colors, moodDominant]);
                //console.log(jsonValue);
                await AsyncStorage.setItem('1', 1)
            } catch (e) {
                console.log("COUCOU")
            }
        }

        let getData = async() => {

            try {
                let date = new date();
                let cle = date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
                const jsonValue = await AsyncStorage.getItem('1');
                this.setState({test:jsonValue});
                console.log(jsonValue.toString());
            } catch(e) {
                console.log("COUCOU2")
            }
        }

        storeData().then(() => getData());*/

        this.setState({fini:true});

    }

    renderItem = ({item}) => <QuestionComponent enonce={item.enonce} id={this.state.id++} onSliderValueChange={this.onSliderValueChange}></QuestionComponent>

    render(){
        if(!this.state.fini){
            return (
            <View style={styles.container}>
                <FlatList styles={styles.questions} data={this.state.listQuestions} renderItem={this.renderItem}
                    keyExtractor={(item,index)=>index.toString()}
                >

                </FlatList>
                <Button
                    title="Valider votre mood !"
                    onPress={this.validerQuestionnaire}
                    styles={styles.bouton}
                />                
            </View>
            )
        } else {
            return (
            <View>
                <Text>{this.state.test}</Text>
            </View>
            )
        }      
    }
}


const styles = StyleSheet.create({
    container: {
        width:"100%",
        justifyContent:"center",
        backgroundColor:"#0bd3d3",
        marginTop:"10%",
        flex:1
    },
    questions:{
        margin:"20%",
        flex:1
    },
    bouton:{
        backgroundColor:"red",
        flex:1
    }
});

export default QuestionnaireComponent
