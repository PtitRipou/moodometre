import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import AccueilComponent from "./AccueilComponent";
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import QuestionnaireComponent from './QuestionnaireComponent';
import MainActivites from './MainActivites';
import DetailsAct from './DetailsAct';

const Drawer = createDrawerNavigator();

class DrawerNavigatorComponent extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            
        }
    }

    render(){
        return (
            <NavigationContainer>
                <Drawer.Navigator initialRouteName="Accueil">
                    <Drawer.Screen name = "Questionnaire" component={QuestionnaireComponent}/>
                    <Drawer.Screen name = "Activites" component={MainActivites} />
                    <Drawer.Screen name = "Accueil" component={AccueilComponent}/>
                </Drawer.Navigator>
            </NavigationContainer>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        width:"100%",
        backgroundColor: "#0bd3d3",
        flex:1
    },
});

export default DrawerNavigatorComponent
