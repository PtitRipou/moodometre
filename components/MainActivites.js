import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import ActivitesComponent from './ActivitesComponent';
import RandomAct from './RandomAct';


class MainActivites extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            ActData: [],
            Activite: [ {nom: 'Sport', temps: '1h', prix: '0€', description: 'Prenez vos meilleurs chaussures et allez courir 1h', image:{uri: 'https://www.alienor-bordeaux.fr/wp-content/uploads/2018/06/sport.jpg'}},
                        {nom: 'Programmation', temps: '3h', prix: '0€', description: 'Apprenez les bases d\'un langage', image:{uri: 'https://culture-informatique.net/wp-content/uploads/2017/08/technology-1283624_640.jpg'}},
                        {nom: 'Rangement', temps: '1h30', prix: '0€', description: 'Rangez votre appartement/maison/chambre', image: {uri: 'https://itsmaeenchantedblog.files.wordpress.com/2015/05/howard-a-du-rangement-a-faire-heureusement.jpg'}},
                        {nom: 'Sortie', temps: '2h', prix: 'Ce que vous voulez', description: 'Sortez prendre un bol d\'air frais et faites vous plaisir', image: {uri: 'https://www.genevepascher.com/wp-content/uploads/2019/02/vieille-ville.jpg'}},
                        {nom: 'Meditation', temps: '15min', prix: '0€', description: 'Relaxez-vous un instant', image:{uri: 'https://www.sante-sur-le-net.com/wp-content/uploads/2018/01/meditation.jpg'}},
                        {nom: 'Programmation', temps: '5min', prix: '0€', description: 'Réalisez un site web, c\'est facile', image:{uri: 'https://culture-informatique.net/wp-content/uploads/2017/08/technology-1283624_640.jpg'}},
                        {nom: 'Programmation', temps: '2ans', prix: '0€', description: 'Deboguez votre site web, c\'est dure', image: {uri:'https://culture-informatique.net/wp-content/uploads/2017/08/technology-1283624_640.jpg'}},
                        {nom: 'Sport', temps: '1h30', prix: '0€', description: 'Allez marcher autour de chez vous', image:{uri: 'https://www.alienor-bordeaux.fr/wp-content/uploads/2018/06/sport.jpg'}},
                        {nom: 'Rangement', temps: '5min', prix: '0€', description: 'Faites du tri sur les applications de votre portables', image: {uri: 'https://itsmaeenchantedblog.files.wordpress.com/2015/05/howard-a-du-rangement-a-faire-heureusement.jpg'}},
                        {nom: 'Art', temps: '2h', prix: '15€', description: 'Visitez un musée ou allez au cinéma', image:{uri: 'https://mbarouen.fr/sites/default/files/styles/page/public/upload/actualites/musee_des_beaux-arts_2_bd.jpg'}},
                        {nom: 'Emploie', temps: '30min', prix: '0€', description: 'Mettez votre CV à jour', image: {uri: 'https://img-4.linternaute.com/kK7OfdPbzODShAEM8EAoN2TzV5Y=/1500x/smart/73356cf9ab694d4d995463496140673c/ccmcms-linternaute/10780854.jpg'}},
                    ],
         }

         this.ActiviteRand()
         //this.showActivities()
    }


    proxyStatelessComponent( props ){
        return (
            <ActivitesComponent  act={props.item} />
        )
    }

    ActiviteRand(){
       
        for (let i=0; i<3; i++) {
            let k = Math.floor(Math.random() * this.state.Activite.length);
            this.state.ActData.push(this.state.Activite[k]);
        }
    }

  /*  showActivities(){
        const ActiviteService = new RandomAct()

        for (let k =0; k<3;k++) {
            ActiviteService.getRandAct().then( randomAct => {
                let currentActData = this.state.ActData;
                currentActData.push(randomAct)
                this.setState({
                    ActData: currentActData

                })
                console.log(currentActData);
            })
        }
    }*/

    render(){
        
        return (
            <View style={styles.container}>
                <View>
                    <FlatList 
                        data={this.state.ActData}
                        renderItem={this.proxyStatelessComponent.bind(this)}
                        keyExtractor={(item, id) => id.toString()}
                     /> 
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({ 
    

      container: {
        flex: 1,
        backgroundColor: '#0bd3d3',
        justifyContent: 'center',
      },
      test: {
          alignContent: 'center',
          alignItems: 'center'
      }
});

export default MainActivites
