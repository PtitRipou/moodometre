import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import CalendarPicker from 'react-native-calendar-picker';

class CalendrierComponent extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            text: "Historique du moodomètre",
            selectedStartDate: null,
        }

        this.onDateChange = this.onDateChange.bind(this);
    }

    determineColor(date) {

    }

    determineMood(date) {

    }

    onDateChange(date) {
        this.setState({
          selectedStartDate: date,
        });
    }

    render(){

        const { selectedStartDate } = this.state;
        const DayColor = selectedStartDate ? determineColor(selectedStartDate) : "grey";
        const DayMood = selectedStartDate ? determineMood(selectedStartDate) : "No data ...";

        return (
            <View>
                <Text style={styles.text}>{this.state.text}</Text>
                <View style={styles.container}>
                    <CalendarPicker
                        onDateChange={this.onDateChange}
                        selectedDayColor={DayColor}
                    />
                    <Text>SELECTED DATE : { DayMood }</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        color: "white",
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 20,
    },

    container: {
        backgroundColor: 'white',
      },
});

export default CalendrierComponent