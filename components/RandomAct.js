import fetch from 'node-fetch';

const RANDOM_ACT_API = "https://www.boredapi.com/api/activity/";

class RandomAct {
  
    getRandAct() {
        return new Promise( (resolve,reject) => {
            fetch(RANDOM_ACT_API).then( httpResponse => {
                httpResponse.json().then( apiResponse => {
                    let newAct = {
                        description: apiResponse.activity,
                        prix: 20*apiResponse.price,
                        nombre: apiResponse.participants,
                    }
                    resolve(newAct)
                })
            })
        })
    }

}

export default RandomAct
